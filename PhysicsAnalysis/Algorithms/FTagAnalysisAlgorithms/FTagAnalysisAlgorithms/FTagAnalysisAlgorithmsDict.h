/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef F_TAG_ANALYSIS_ALGORITHMS__F_TAG_ANALYSIS_ALGORITHMS_DICT_H
#define F_TAG_ANALYSIS_ALGORITHMS__F_TAG_ANALYSIS_ALGORITHMS_DICT_H

#include <FTagAnalysisAlgorithms/BTaggingEfficiencyAlg.h>
#include <FTagAnalysisAlgorithms/BTaggingInformationDecoratorAlg.h>
#include <FTagAnalysisAlgorithms/BTaggingScoresAlg.h>
#include <FTagAnalysisAlgorithms/XbbEfficiencyAlg.h>
#include <FTagAnalysisAlgorithms/XbbInformationDecoratorAlg.h>

#endif
