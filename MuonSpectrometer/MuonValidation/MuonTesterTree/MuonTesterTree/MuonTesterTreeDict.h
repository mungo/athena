/*
Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONTESTER_MUONTESTERTREEDICT_H
#define MUONTESTER_MUONTESTERTREEDICT_H
#include <MuonTesterTree/ArrayBranch.h>
#include <MuonTesterTree/AuxElementBranch.h>
#include <MuonTesterTree/CoordTransformBranch.h>
#include <MuonTesterTree/EventHashBranch.h>
#include <MuonTesterTree/EventIDBranch.h>
#include <MuonTesterTree/EventInfoBranch.h>
#include <MuonTesterTree/FourVectorBranch.h>
#include <MuonTesterTree/GenericDecorBranch.h>
#include <MuonTesterTree/IMuonTesterBranch.h>
#include <MuonTesterTree/IParticleFourMomBranch.h>
#include <MuonTesterTree/IdentifierBranch.h>
#include <MuonTesterTree/LinkerBranch.h>
#include <MuonTesterTree/MatrixBranch.h>
#include <MuonTesterTree/MuonTesterBranch.h>
#include <MuonTesterTree/MuonTesterTree.h>
#include <MuonTesterTree/ScalarBranch.h>
#include <MuonTesterTree/SetBranch.h>
#include <MuonTesterTree/ThreeVectorBranch.h>
#include <MuonTesterTree/TrackChi2Branch.h>
#include <MuonTesterTree/TwoVectorBranch.h>
#include <MuonTesterTree/VectorBranch.h>
#endif

