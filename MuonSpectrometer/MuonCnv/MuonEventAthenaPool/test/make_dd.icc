/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
/**
 * @file MuonEventAthenaPool/test/make_dd.icc
 * @author scott snyder <snyder@bnl.gov>
 * @date Nov, 2016
 * @brief Helper for regression tests.
 */


#include "MuonIdHelpers/RpcIdHelper.h"
#include "RPC_CondCabling/RpcCablingCondData.h"
#include "RPC_CondCabling/RDOindex.h"
#include "IdDictParser/IdDictParser.h"
#include "StoreGate/StoreGateSvc.h"
#include "AthenaKernel/CondCont.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/EventContext.h"
#include "GaudiKernel/ThreadLocalContext.h"

void make_dd (IdDictParser& parser)
{
  SmartIF<StoreGateSvc> sg{Gaudi::svcLocator()->service("DetectorStore")};
  assert ( sg.isValid() );

  parser.register_external_entity ("InnerDetector",
                                   "IdDictInnerDetector.xml");
  parser.register_external_entity ("MuonSpectrometer",
                                   "IdDictMuonSpectrometer_S.02.xml");
  parser.register_external_entity ("Calorimeter",
                                   "IdDictCalorimeter_L1Onl.xml");
  parser.register_external_entity ("LArCalorimeter",
                                   "IdDictLArCalorimeter.xml");
  IdDictMgr& idd = parser.parse ("IdDictParser/ATLAS_IDS.xml");


  auto rpc_id = std::make_unique<RpcIdHelper>();
  rpc_id->initialize_from_dictionary (idd);
  if (sg->record (std::move (rpc_id), "RPCIDHELPER").isFailure()) std::abort();

}


class TestRCUSvc
  : public implements<Athena::IRCUSvc>
{
public:
  virtual StatusCode remove (Athena::IRCUObject* obj) override
  {
    m_removed = obj;
    return StatusCode::SUCCESS;
  }
  virtual size_t getNumSlots() const override
  { return 1; }
  virtual void add (Athena::IRCUObject* /*obj*/) override
  { }

  Athena::IRCUObject* m_removed = nullptr;
};


EventIDBase runlbn (int run, int lbn)
{
  return EventIDBase (run,
                      EventIDBase::UNDEFEVT,  // event
                      EventIDBase::UNDEFNUM,  // timestamp
                      EventIDBase::UNDEFNUM,  // timestamp ns
                      lbn);
}


void make_cond (TestRCUSvc& rcusvc,
                const std::vector<Identifier>& ids = std::vector<Identifier>())
{
  EventContext ctx;
  ctx.setEventID (runlbn (1, 1));
  Atlas::setExtendedEventContext (ctx, Atlas::ExtendedEventContext (nullptr));
  Gaudi::Hive::setCurrentContext (ctx);

  auto data = std::make_unique<RpcCablingCondData>();
  // RpcPadContainerCnv uses only a couple things from RpcCablingCondData.
  std::vector<Identifier> v(500);
  data->setIds (v);
  data->setLookup (ids);

  std::string key = "RpcCablingCondData";
  EventIDRange r (runlbn(0, 0), runlbn (10000000, 0));
  DataObjID id ("RpcCablingCondData", key);
  auto cc = std::make_unique<CondCont<RpcCablingCondData> > (rcusvc, id);
  if (cc->insert (r, std::move(data), ctx).isFailure()) std::abort();

  ServiceHandle<StoreGateSvc> condStore ("StoreGateSvc/ConditionStore", "test");
  assert (condStore.retrieve().isSuccess());
  if (condStore->record (std::move (cc), key).isFailure()) std::abort();
}

